import pandas as pd
import seaborn as sns

# train.csv veri setinin pandas aracılığı ile okunması.
df=pd.read_csv('train.csv')

# MSZoning sütunun'da data_description'a aykırı düzensizliklerin giderilmesi.
df.loc[df.MSZoning == "C (all)","MSZoning"] = "C"

# LotFrontage için "None" value lerin 0 olabileceğini düşündüm.
# Linear feet of street connected to property.
df.loc[df.LotFrontage == "NA","LotFrontage" ] = 0
df.loc[pd.isna(df["LotFrontage"]),"LotFrontage"] = 0

# Ev fiyatlarında bağlı olduğu yolun tipinin etkili olmadığını düşündüm.
df.drop(['Street'], axis = 1, inplace=True)
df.drop(['Alley'], axis = 1, inplace=True)

# Sadece 1 tane NoSeWa, 1459 tane AllPub olmasından dolayı kaldırdım.
df.drop(['Utilities'], axis = 1, inplace=True)

# BldgType sütunun'da data_description'a aykırı düzensizliklerin giderilmesi.
df.loc[df.BldgType == "2fmCon","BldgType"] = "2FmCon" 
df.loc[df.BldgType == "Duplex","BldgType"] = "Duplx" 

#Exterior2nd sütunun'da data_description'a aykırı düzensizliklerin giderilmesi.
df.loc[df.Exterior2nd == "CmentBd","Exterior2nd"] = "CemntBd"  
df.loc[df.Exterior2nd == "Wd Shng","Exterior2nd"] = "Wd Sdng"
df.loc[df.Exterior2nd == "Brk Cmn","Exterior2nd"] = "BrkComm"

#None value lerin "None"(String) şeklinde düzenlenmesi.
df.loc[pd.isna(df["MasVnrType"]),"MasVnrType"] = "None"

#None value lerin 0 şeklinde düzenlenmesi.
df.loc[pd.isna(df["MasVnrArea"]),"MasVnrArea"] = 0

df.loc[pd.isna(df["BsmtQual"]),"BsmtQual"] = "None"
df.loc[pd.isna(df["BsmtCond"]),"BsmtCond"] = "None"
df.loc[pd.isna(df["BsmtExposure"]),"BsmtExposure"] = "None"
df.loc[pd.isna(df["BsmtFinType1"]),"BsmtFinType1"] = "None"
df.loc[pd.isna(df["BsmtFinType2"]),"BsmtFinType2"] = "None"

#Elektrical sütunun'da None değere sahip olan veri satırını kaldırdım.
df = df[df['Electrical'].notna()]

df.loc[pd.isna(df["FireplaceQu"]),"FireplaceQu"] = "None"
df.loc[pd.isna(df["GarageType"]),"GarageType"] = "None"

# GarageYrBlt sütununun gereksiz olduğunu düşündüğümden kaldırdım.
df.drop(['GarageYrBlt'], axis = 1, inplace=True)

df.loc[pd.isna(df["GarageFinish"]),"GarageFinish"] = "None"
df.loc[pd.isna(df["GarageQual"]),"GarageQual"] = "None"
df.loc[pd.isna(df["GarageCond"]),"GarageCond"] = "None"
df.loc[pd.isna(df["PoolQC"]),"PoolQC"] = "None"
df.loc[pd.isna(df["Fence"]),"Fence"] = "None"
df.loc[pd.isna(df["MiscFeature"]),"MiscFeature"] = "None"

sns.heatmap(df.isnull(),yticklabels=False,cbar=False,cmap='YlGnBu')

# Kategorik değişkenleri kukla / gösterge değişkenlere dönüştürdüm. 
df = pd.get_dummies(df, drop_first=True)

# None tipi sütunları kaldırdım.
df=df.drop(['MiscFeature_None'], axis=1)
df=df.drop(['Fence_None'], axis=1)
df=df.drop(['PoolQC_None'], axis=1)
df=df.drop(['GarageCond_None'], axis=1)
df=df.drop(['GarageQual_None'], axis=1)
df=df.drop(['GarageFinish_None'], axis=1)
df=df.drop(['GarageType_None'], axis=1)
df=df.drop(['FireplaceQu_None'], axis=1)
df=df.drop(['BsmtFinType2_None'], axis=1)
df=df.drop(['BsmtFinType1_None'], axis=1)
df=df.drop(['BsmtExposure_None'], axis=1)
df=df.drop(['BsmtCond_None'], axis=1)
df=df.drop(['BsmtQual_None'], axis=1)
df=df.drop(['MasVnrType_None'], axis=1)

# Data Frame'in son hâlini, final.csv şeklinde kaydettim.
df.to_csv('final.csv')