import pandas as pd
import xgboost as xgb
from xgboost import XGBRegressor
from sklearn.model_selection import train_test_split, GridSearchCV,cross_val_score
from sklearn.metrics import mean_squared_error, r2_score
import numpy as np
from sklearn import model_selection

# final.csv'nin okunması.
df=pd.read_csv('final.csv')
# Data Frame içerisinde gereksiz sütun ların kaldırılması.
df.drop("Unnamed: 0", axis = 1, inplace = True)
df.drop("Id", axis = 1, inplace = True)

# Bağımsız değişkenlerin x'e eşitlenmesi. 
x=df.drop(["SalePrice"], axis = 1)
# Bağımlı değişkenlerin y'e eşitlenmesi.
y=df["SalePrice"]

# Train ve Test split in yapılması.
X_train, X_test, y_train, y_test = train_test_split(x, y, test_size = 0.80, random_state =1)

# XGBoost için en iyi parametrelerin bulunması.
'''
params = {"colsample_bytree":[0.2,0.3,0.4,0.5,0.6],
         "learning_rate":[0.01,0.02,0.03,0.05,0.09],
         "max_depth":[2,3,4,4.5,5,5.5,6],
         "n_estimators":[100,200,350,500,750,2000]}
         
xgb = XGBRegressor()
grid = GridSearchCV(xgb, params, cv = 10, n_jobs = -1, verbose = 2)
grid.fit(X_train, y_train)
grid.best_params_
'''

# En iyi parametreler ile XGBoost'un tanımlanması.
xgb1 = XGBRegressor(colsample_bytree = 0.4, learning_rate = 0.02, max_depth = 5, n_estimators = 500)
# Model'in eğitilmesi.
model_xgb = xgb1.fit(X_train, y_train)

# Eğitilen model ile Test verisinin ilk 25 değerinin gerçek değerler ile grafiğe dökülmesi.
from pylab import *
new_test=[]
for i in y_test:
  new_test.append(i)
  
plot(new_test[0:25])
print(new_test[0:5])
print(model_xgb.predict(X_test)[0:5])
plot(model_xgb.predict(X_test)[0:25])
show()

# Modelin Test skoru.
model_xgb.score(X_test, y_test)

# Modelin Train skoru.
model_xgb.score(X_train, y_train)

# Model için veri setindeki hangi sütunların en etkili olduğunun görülmesi.
importances=pd.DataFrame({"Importance": model_xgb.feature_importances_},index=X_train.columns)

plot(importances[:])
show()